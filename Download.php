<?php

namespace [Vendor Name]\[Your Module]\Controller\Download;

use Magento\Framework\View\Result\PageFactory;

class Download extends \Magento\Framework\App\Action\Action
{
    protected $productCollectionFactory;

    public function __construct( 
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory ) 
    {
        $this->productCollectionFactory = $productCollectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $notificationId = $this->getRequest()->getParam('notification_id');
        $heading = [__('Id'),__('SKU'),__('Name')];
        //$heading = [__('Brand'),__('Name'),__('Description'),__('SKU'),__('Standard Cost'),__('Cost Ex GST'),__('Cost Inc GST'),__('RRP'),__('P&H Ex GST'),__('P&H Inc GST'),__('Stock on Hand'),__('Status'),__('Comments')];        
        $outputFile = "ListProducts". date('Ymd_His').".csv";
        $handle = fopen($outputFile, 'w');
        fputcsv($handle, $heading);
        foreach ($productCollection as $product) { 
            $row = [
                $product->getId(),
                $product->getSku(),
                $product->getName()
                //$product->getData('brand'),
                //$product->getData('name'),
                //$product->getData('description'),
                //$product->getData('sku'),
                //$product->getData('cost_standard'),
                //$product->getData('cost'),
                //$product->getData('cost') * 1.1,
                //$product->getData('price_store'),
                //$product->getData('freight_cost'),
                //$product->getData('freight_cost') * 1.1,          
                //$product->getData('soh_qty_tp'),
                //$product->getData('status'),
                //$product->getData('product_comments')                
            ];
            fputcsv($handle, $row);
        }
        $this->downloadCsv($outputFile);
    }
    public function downloadCsv($file)
    {
        if(file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();flush();
            readfile($file);
        }
    }
}